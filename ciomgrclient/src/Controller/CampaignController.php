<?php


namespace App\Controller;


use App\Servies\FileSystemServices;
use App\Servies\SettingServices;
use DirectoryIterator;
use DOMDocument;
use DOMXPath;
use JsonException;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\Finder\Finder;
use ZipArchive;


class CampaignController extends AbstractController
{
    /**
     * @var FileSystemServices
     */
    private $fileSystemServices;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SettingServices
     */
    private $settingService;


    public function __construct(
        FileSystemServices $fileSystemServices,
        LoggerInterface    $logger,
        SettingServices    $settingService
    )
    {
        $this->fileSystemServices = $fileSystemServices;
        $this->logger = $logger;
        $this->settingService = $settingService;
    }


    /**
     * Route No. 1,
     * @Route( "/create", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author pva
     */
    public function createCampaign(Request $request, FileSystemServices $fileSystemServices): Response
    {
        $company = $request->get('company');
        $campaign_name = $request->get('campaign_name');
        $token = $request->get('token');
        $useCaseType = $request->get('useCaseType');
        $response = new Response();

        if (empty($company) || empty($campaign_name) || empty($token) || empty($useCaseType)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign name, token or use case type.']));
        }

        $status = $fileSystemServices->createCampaign($campaign_name, $company, $token, $useCaseType);
        return $response->setContent(json_encode($status));
    }

    /**
     * Route No. 2,
     * @Route( "/activate", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author pva
     */
    public function activateCampaign(Request $request, FileSystemServices $fileSystemServices): Response
    {
        $company = $request->get('company');
        $campaign_name = $request->get('campaign_name');
        $version = $request->get('version');
        $campaignSettings = $request->get('campaignSettings');
        $response = new Response();

        if (empty($company) || empty($campaign_name) || empty($version) || empty($campaignSettings)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign name, version or campaign settings.']));
        }

        $status = $fileSystemServices->activateCampaign($campaign_name, $company, $version, $campaignSettings);
        return $response->setContent(json_encode($status));
    }

    /**
     * Route No. 3,
     * @Route( "/revise", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author pva
     */
    public function reviseCampaign(Request $request, FileSystemServices $fileSystemServices): Response
    {
        $company = $request->get('company');
        $campaign_name = $request->get('campaign_name');
        $version_old = $request->get('version_old');
        $version_new = $request->get('version_new');
        $token = $request->get('token');
        $response = new Response();

        if (empty($company) || empty($campaign_name) || empty($version_old) || empty($version_new) || empty($version_new) || empty($token)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign name, version old, version new or token.']));
        }

        $status = $fileSystemServices->reviseCampaign($campaign_name, $company, $version_old, $version_new, $token);//we need to pass token
        return $response->setContent(json_encode($status));
    }


    /**
     * Route No. 4,
     * @Route( "/create_splittest", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author Pradeep
     */
    public function createSplittest(Request $request, FileSystemServices $fileSystemServices): Response
    {
        $company = $request->get('company');
        $campaign_name = $request->get('campaign_name');
        $version = $request->get('version');
        $splittest_name = $request->get('splittest_name');
        $response = new Response();

        if (empty($company) || empty($campaign_name) || empty($version) || empty($splittest_name)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign name, version or splittest name.']));
        }

        $status = $fileSystemServices->createSplittest($company, $campaign_name, $version, $splittest_name);
        return $response->setContent(json_encode($status));
    }

    /**
     * Route No. 5,
     * @Route( "/copy", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author Pradeep
     */
    public function copyCampaign(Request $request, FileSystemServices $fileSystemServices): Response
    {
        $company = $request->get('company');
        $campaign_old = $request->get('campaign_old');
        $version = $request->get('version');
        $campaign_new = $request->get('campaign_new');
        $token = $request->get('token');
        $response = new Response();

        if (empty($company) || empty($campaign_old) || empty($version) || empty($campaign_new) || empty($token)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign old, campaign new or token.']));
        }

        $status = $fileSystemServices->copyCampaign($company, $campaign_old, $version, $campaign_new, $token);
        return $response->setContent(json_encode($status));
    }

    /**
     * Route No. 6,
     * @Route( "/createFlowControl", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author Pradeep
     */
    public function createFlowControlPages(Request $request, FileSystemServices $fileSystemServices): Response
    {
        $company = $request->get('company');
        $campaign_name = $request->get('campaign_name');
        $version = $request->get('version');
        $additional_pages = $request->get('pages');
        $response = new Response();

        if (empty($company) || empty($campaign_name) || empty($version) || empty($additional_pages)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign name, version or additional pages.']));
        }

        $status = $fileSystemServices->createAdditionalPages($campaign_name, $company, $version, $additional_pages);
        return $response->setContent(json_encode($status));
    }

    /**
     * @Route( "/page/download", methods={"POST"})
     * @param Request $request
     * @return BinaryFileResponse
     * @throws \JsonException
     * @author Aki
     */
    public function downloadPageFolderAsZip(Request $request): Response
    {
        // Provide a name for your file with extension
        //get the content from the request
        $encodedContent = $request->get('download_details');//getData
        $pageSettingsEncoded = $request->get('page_settings');//getData

        //basic check
        if (empty($encodedContent)) {
            $errorResponse = new Response();
            return $errorResponse->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for download details']));
        }

        //decrypt the content
        $decodedContent = base64_decode($encodedContent);
        $pageDetails = json_decode($decodedContent, true, 512, JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);
        $this->logger->info('Download request for page', [$decodedContent, __METHOD__, __LINE__]);
        $pageSettings =  base64_decode($pageSettingsEncoded);

        $folderDir = $this->fileSystemServices->generatePathForFolder($pageDetails);
        //required folders
        $files = new DirectoryIterator($folderDir);
        $javascriptFiles = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($folderDir . "/scripts"),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        $styleFiles = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($folderDir . "/styles"),
            RecursiveIteratorIterator::LEAVES_ONLY
        );
        //generate zip-file name
        $zipName = $pageDetails['folder_name'] . '.zip';
        $zip = new ZipArchive;
        if ($zip->open($zipName, ZipArchive::CREATE) === TRUE) {
            //add HTML files
            foreach ($files as $file) {
                if (!$file->isDot()) {
                    $fileExtension = $file->getExtension();
                    if ($fileExtension === 'html') {
                        // Get real and relative path for current file
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($folderDir) + 1);
                        // Change the form.html according to WEB-4524
                        $fileName = $file->getBasename();
                        if(in_array($fileName,$this->settingService->filesAllowedForDownload())){
                            //todo:remove this if else after templates are generated dynamically
                            //todo:Just get files form the folder no injection of data
                            /*
                            if($fileName === SettingServices::INDEX_HTML){
                                $formattedData = $this->settingService->formatDataForTwig($pageSettings);
                                $renderedFormHtml = $this->renderView('index.twig',
                                    ['mapping' => $formattedData]);
                                $zip->addFromString(SettingServices::INDEX_HTML,$renderedFormHtml);
                            }else{
                            */
                                // Add current file to archive
                                $zip->addFile($filePath, $relativePath);
                            //}
                        }
                    }
                }
            }
            //adding JS files
            foreach ($javascriptFiles as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($folderDir) + 1);
                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }
            //adding CSS files
            foreach ($styleFiles as $name => $file) {
                // Skip directories (they would be added automatically)
                if (!$file->isDir()) {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($folderDir) + 1);
                    // Add current file to archive
                    $zip->addFile($filePath, $relativePath);
                }
            }

            // All files are added, so close the zip file.
            $zip->close();
        }
        //adding headers for the file
        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
        $response->headers->set('Content-length', filesize($zipName));
        $response->headers->set('Content-Transfer-Encoding', 'binary');

        unlink($zipName);
        return $response;

    }

    /**
     * @Route( "/page/upload", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @author Aki
     */
    public function uploadPages(Request $request): JsonResponse
    {
        //get file form request
        $uploadedFile = $request->files->get('zipFile'); // get zip Archive object of UploadedFile::class


        if ($uploadedFile->getClientOriginalExtension() !== "zip") {
            return $this->json([
                'status' => false,
                'message' => 'Please upload zip file',
            ]);
        }

        // page_upload_dir is configured in services.yaml
        $uploadDir = $this->getParameter('page_upload_dir'); // ../files/upload_pages
        //get parameters form request
        $version = $request->get('version');
        $folderName = $request->get('folderName');
        $company = $request->get('company');

        //download file to upload folder
        if ('zip' !== $uploadedFile->getClientOriginalExtension()) {
            return $this->json([
                'status' => false,
                'message' => 'File extension is not "zip": You can upload zip files only.',
            ]);
        }

        $zipFIleName = $folderName . "." . $uploadedFile->getClientOriginalExtension();


        $sourceFolder = $uploadDir . DIRECTORY_SEPARATOR . $folderName; // ../files/upload_pages/test01 if a test01.zip was uploaded
        $uploadedFile->move($sourceFolder, $zipFIleName); // ../files/upload_pages/test01/test01.zip

        //relative path
        $relativePath = $company . DIRECTORY_SEPARATOR . $folderName . DIRECTORY_SEPARATOR . "versions" . DIRECTORY_SEPARATOR . $version;

        $this->logger->info('VARS', [$relativePath, $sourceFolder, $zipFIleName, $uploadDir, $version, $folderName, $company]);
        //  VARS ["1002/eefw/versions/2","../files/upload_pages/eefw","eefw.zip","../files/upload_pages","2","eefw","1002"]

        $sourceUnzipFolder = $this->fileSystemServices->unZip($sourceFolder, $zipFIleName, $uploadDir) . DIRECTORY_SEPARATOR . $folderName; // returns /var/www/.../files/upload_pages/test01



        try {
            $indexHtmlFolder = $this->fileSystemServices->getAbsolutePath('index.html', $sourceUnzipFolder);
        } catch (\Exception $e) {
            return $this->json([
                'status' => false,
                'message' => $e->getMessage(),
            ]);
        }

        $this->logger->info('FOLDERS', ['$sourceUnzipFolder' => $sourceUnzipFolder, '$indexHtmlFolder' => $indexHtmlFolder, __METHOD__, __LINE__]);
        /*
         * todo:change this for the dynamically genered form or if settings are saved in index.html file
        //remove form elements generated dynamically
        $dom = new DOMDocument('1.0', 'UTF-8');
        $internalErrors = libxml_use_internal_errors(true);// set error level
        $dom->loadHTMLFile($sourceUnzipFolder.'/index.html');
        libxml_use_internal_errors($internalErrors);
        $xpath = new DOMXPath($dom); // Restore error level
        $nodes = $xpath->query('//div[@id="generatedForm"]');
        if($nodes->length){
            $nodes[0]->parentNode->removeChild($nodes[0]);
        }
        $dom->saveHTMLFile($sourceUnzipFolder.'/index.html');
        */

        //  FOLDERS {"$sourceUnzipFolder":"/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/files/upload_pages","$indexHtmlFolder":"/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/files/upload_pages/MSCAMPTEST","0":"App\\Controller\\CampaignController::uploadPages","1":320}
        if ($sourceUnzipFolder === $indexHtmlFolder) {

            //Extract files
            $moveFilesToPageFolder = $this->fileSystemServices->moveUnzipedFiles($indexHtmlFolder, $relativePath);
            $this->logger->info('moveUnzipedFiles', ['$indexHtmlFolder' => $indexHtmlFolder, '$relativePath' => $relativePath, '$moveFilesToPageFolder' => $moveFilesToPageFolder, __METHOD__, __LINE__]);

            if (!$moveFilesToPageFolder) {
                return $this->json([
                    'status' => false,
                    'message' => 'files not successfully moved',
                ]);
            }

            //delete uploaded local file
            $deleteLocalZipFile = $this->fileSystemServices->deleteTempFile($uploadDir);
            if (!$deleteLocalZipFile) {
                $this->logger->error('Delete temp files');
            }
            //return success
            return $this->json([
                'status' => true, // TODO: true
                'message' => 'Page upload successful',
                'new_relative_path' => $relativePath
            ]);
        } else {
            return $this->json([
                'status' => false,
                'message' => sprintf('Page upload failed: index.html was not found in the expected folder "%s". Please check your zip-archive file & directory structure', $folderName)
            ]);
        }
    }


    /**
     * @Route( "/check/upl", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \JsonException
     * @author jsr XXXX4
     */
    public function uploadTest(Request $request): JsonResponse
    {


        //get file form request
        $all = $request->request->keys();
        // $this->logger->info('$request->request->all()', [$request->request->all()]);

        // $uploadFile = new UploadedFile($this->getParameter('page_upload_dir'), $request->files->get('filename'));
        return $this->json(['all' => $all]);
    }


    /**
     * @Route( "/settings/update", methods={"POST"})
     * @param Request $request
     * @param FileSystemServices $fileSystemServices
     * @return Response
     * @author  Aki
     * @internal This method updates settings.txt file and form.html of client for the version provided
     * todo:inject settings data to index.html and add php file to send the data
     */
    public function addOrUpdateClientSettings(Request $request, FileSystemServices $fileSystemServices): Response
    {
        //get values from request
        $company = $request->get('company');
        $campaign_name = $request->get('campaign_name');
        $version = $request->get('version');
        $campaignSettings = $request->get('campaignSettings');
		$urlImprint = $request->get('urlImprint', '#');
		$urlPrivacyPolicy = $request->get('urlPrivacyPolicy', '#');
        $response = new Response();

        //basic check of the request params
        if (empty($company) || empty($campaign_name) || empty($version) || empty($campaignSettings)) {
            return $response->setContent(json_encode(['status' => false, 'message' => 'Invalid or missing parameter values for company, campaign name, version or campaign settings.']));
        }

        //required paths variables
        $companyPath = $this->fileSystemServices->getdirPath() . DIRECTORY_SEPARATOR . $company;
        $versionPath = $companyPath . DIRECTORY_SEPARATOR . $campaign_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR;

        //update the form.html in client (Requirements changed WEB-5484 , WEB- 5466 )

        /*
         * @internal: we no longer need to inject data , form.html is deprecated
        try {
            //getting mapping table from settings
            $mappingTable = json_decode($campaignSettings, true, 512,JSON_THROW_ON_ERROR)['mapping']['mappingTable'];
            $this->logger->info('mapping table', [$mappingTable, __METHOD__, __LINE__]);
            //Decoding base64 encoded mapping
            $mapping = json_decode(base64_decode($mappingTable), true, 512, JSON_THROW_ON_ERROR);
            $this->logger->info('mapping', [$mapping, __METHOD__, __LINE__]);
            //Format the mapping to twig friendly structure
            $formatMappingForTwig = $this->settingService->formatMappingForTwig($mapping);
            $this->logger->info('formatted Mapping for twig', [$formatMappingForTwig, __METHOD__, __LINE__]);
            //render the form html in string
            $renderedFormHtml = $this->renderView('index.twig', ['mapping' => $formatMappingForTwig,
                'urlImprint' => $urlImprint,
                'urlPrivacyPolicy' => $urlPrivacyPolicy,
                'campaignSettings' => base64_encode($campaignSettings),
            ]);
            //Update the form html file in the client
            $this->fileSystemServices->addMappingFieldsToFormHtml($versionPath, $renderedFormHtml);
        } catch (JsonException $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $response->setContent(json_encode(['status' => false, 'message' => $e->getMessage()]));
        }
        */

        //update the setting.txt file
        $status = $this->fileSystemServices->addSettingsFileAndFormFieldsToCampaign($versionPath, $campaignSettings);

        //return response
        return $response->setContent(json_encode($status));
    }


}
