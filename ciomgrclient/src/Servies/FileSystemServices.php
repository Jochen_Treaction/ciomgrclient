<?php


namespace App\Servies;


use DirectoryIterator;
use Exception;
use FilesystemIterator;
use Psr\Log\LoggerInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Filesystem\Filesystem;
use ZipArchive;

/**
 * @author  pva
 * Class FileSystemServices
 * @package App\Servies
 */
class FileSystemServices
{

    /**
     * @var Filesystem
     */
    private $file_system;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var string
     */
    private $mclient;
    /**
     * @var string
     */
    private $dir;
    private $webhookClient;


    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->file_system = new Filesystem();
        $this->dir = $_ENV['CAMPAIGN_DIR'];
        $this->mclient = $_ENV['JS_CLIENT'];
        $this->webhookClient = $_ENV['WEBHOOK_CLIENT'];
    }


    public function runtest($company, $campaign): array
    {
        /*
        0 == --- == no access
        1 == --x == execute
        2 == -w- == write
        3 == -wx == write / execute
        4 == r-- == read
        5 == r-x == read / execute
        6 == rw- == read / write
        7 == rwx == read / write / execute
        */

        $this->logger->info("DIRS: ", [$this->dir, $company, $campaign], [__METHOD__, __LINE__]);
        $err = [];

        // company / campaign
        try {
            //$this->logger->info("makePathRelative from {$this->dir} this path $company/$campaign" ,[__METHOD__,__LINE__]);
            //$this->file_system->makePathRelative('/'. $company . '/' . $campaign, $this->dir);
            $this->logger->info("mkdir 0755 {$this->dir}/{$campaign}/{$company}", [__METHOD__, __LINE__]);
            $this->file_system->mkdir($this->dir . DIRECTORY_SEPARATOR . $company, 0755);
            $this->file_system->mkdir($this->dir . DIRECTORY_SEPARATOR . $company . DIRECTORY_SEPARATOR . $campaign, 0755);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $err[] = [$e->getMessage(), __METHOD__, __LINE__];
        }

        try {
            $this->file_system->mirror($this->mclient, $this->dir . DIRECTORY_SEPARATOR . $company . DIRECTORY_SEPARATOR . $campaign);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        // campaign / company
        try {
            $this->logger->info("mkdir 0755 {$this->dir}" . DIRECTORY_SEPARATOR . $campaign . DIRECTORY_SEPARATOR . "$company", [__METHOD__, __LINE__]);
            $this->file_system->mkdir($this->dir . DIRECTORY_SEPARATOR . $campaign . DIRECTORY_SEPARATOR . $company, 0755);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $err[] = [$e->getMessage(), __METHOD__, __LINE__];
        }

        try {
            $this->logger->info("chmod 755 $company", [__METHOD__, __LINE__]);
            $this->file_system->chmod($this->dir . DIRECTORY_SEPARATOR . $campaign, 0755);
            $this->file_system->chmod($this->dir . DIRECTORY_SEPARATOR . $campaign . DIRECTORY_SEPARATOR . $company, 0755);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            $err[] = [$e->getMessage(), __METHOD__, __LINE__];
        }

        try {
            $this->file_system->mirror($this->mclient, $this->dir . DIRECTORY_SEPARATOR . $campaign . DIRECTORY_SEPARATOR . $company);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

//        try {
//            $this->logger->info("chmod 744 {$this->dir}/{$company}/$campaign",[__METHOD__,__LINE__]);
//            $this->file_system->chmod($this->dir.'/'.$company . '/' . $campaign, 0740);
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(),[__METHOD__,__LINE__]);
//            $err[] = [$e->getMessage(), __METHOD__,__LINE__];
//        }

        return $err;
    }


    /**
     * @param string $camp_name
     * @param string $company
     * @param string $token
     * @param string $useCaseType
     * @return bool
     * @author pva
     */
    public function createCampaign(string $camp_name, string $company, string $token, string $useCaseType): bool
    {
        $this->logger->info('master this-mclient' . $this->mclient, [__METHOD__, __LINE__]);
        $this->logger->info('this-dir: ' . $this->dir, [__METHOD__, __LINE__]);
        $this->logger->info('useCaseType: ' . $useCaseType, [__METHOD__, __LINE__]);

        $status = false;
        if (empty($camp_name) || empty($company) || empty($token) || empty($useCaseType)) {
            $this->logger->critical('Invalid Params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        if (empty($this->mclient) || empty($this->file_system)) {
            $this->logger->critical('Empty Path or Filesystem', [__METHOD__, __LINE__]);
            return $status;
        }
        if (!$this->isCompanyExists($this->dir, $company)) {
            // $this->logger->critical('Folder ' . $company . ' not present', [__METHOD__, __LINE__]);
            $this->logger->info('Creating Folder ' . $company, [__METHOD__, __LINE__]);
            $retCreateFolder = $this->createFolder($this->dir, $company);
            $this->logger->info('result createFolder ' . $retCreateFolder, [__METHOD__, __LINE__]);

            if (!$retCreateFolder) {
                return $status;
            } else {
                ; // go on
            }
        }

        if ($useCaseType === 'webhook') {
            try {
                $company_path = $this->dir . DIRECTORY_SEPARATOR . $company;        // /var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/<company>
                $rel_campaign_path = $camp_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR;   // <campaign>/versions/1/
                $campaign_path = $company_path . DIRECTORY_SEPARATOR . $rel_campaign_path;// create campaign dir.
                if ($this->createDir($company_path, $rel_campaign_path) && $this->copyCampaignFolder($this->webhookClient, $campaign_path)) {
                    //Append Token to File.
                    $this->file_system->dumpFile($campaign_path . DIRECTORY_SEPARATOR . 'token.txt', $token);
                    $status = true;
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }
            return $status;
        }

        try {
            $company_path = $this->dir . DIRECTORY_SEPARATOR . $company;        // /var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/<company>
            $rel_campaign_path = $camp_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR;   // <campaign>/versions/1
            $campaign_path = $company_path . DIRECTORY_SEPARATOR . $rel_campaign_path;// create campaign dir.
            if ($this->createDir($company_path, $rel_campaign_path) && $this->copyCampaignFolder($this->mclient, $campaign_path)) {
                /*
                 * @internal :token no longer required
                Append Token to File.
                $this->file_system->dumpFile($campaign_path . DIRECTORY_SEPARATOR . 'token.txt', $token);

               try {
                    $this->file_system->chmod($company_path, 0755, 0000, true);// todo: bugfix that, since not working
                } catch (Exception $e) {
                    $this->logger->error(strtoupper($company_path).': '. $e->getMessage(), [__METHOD__,__LINE__]);
                }

                try {
                    $this->file_system->chmod($this->dir . '/' . $company, 0755, 0000, true); // todo: bugfix that, since not working
                } catch (Exception $e) {
                    $this->logger->error(strtoupper($this->dir . '/' . $company).': '.$e->getMessage(), [__METHOD__,__LINE__]);
                }
                */

                $status = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $domain
     * @param string $company
     * @return bool
     */
    public function isCompanyExists(string $domain, string $company): bool
    {
        $status = false;
        if (empty($domain) || empty($company)) {
            $this->logger->critical('Invalid Params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $company_path = $domain . DIRECTORY_SEPARATOR . $company;
            $status = $this->exists($company_path);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $path
     * @return bool
     */
    private function exists(string $path): bool
    {
        $status = false;
        if (empty($path) || empty($this->file_system)) {
            $this->logger->critical('Empty Path or Filesystem', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $status = $this->file_system->exists($path);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $path
     * @param string $dir
     * @return bool
     * @author pva
     * @author jsr, 2020-03-17
     */
    private function createDir(
        string $path,
        string $dir
    ): bool // path = /var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/<company> ,  dir = <campaign>/versions/1
    {
        $this->logger->info('createDir: ', ['$path' => $path, '$dir' => $dir], [__METHOD__, __LINE__]);

        try {
            $this->file_system->chmod($path, 0777);
        } catch (Exception $e) {
            $this->logger->error('CHMOD 777 ERROR ' . $path, [__METHOD__, __LINE__]);
        }

        $status = false;
        if (empty($path) || empty($dir) || !$this->exists($path)) {
            return $status;
        }
        $new_path = $path . DIRECTORY_SEPARATOR . $dir;
        try {
            $this->logger->info('create dir path ' . $new_path, [__METHOD__, __LINE__]);

            $this->file_system->mkdir($new_path);

            if (!$this->exists($new_path)) {
                $this->logger->error('MKDIR ERROR ' . $new_path, [__METHOD__, __LINE__]);
                $status = false;
            } else {
                $ret = $this->setPathPerms($path, $dir); // change each path-segment below subdomain to 0755

                if (!empty($ret)) {
                    $this->logger->error('CHMOD ERRORS: ', $ret, [__METHOD__, __LINE__]);
                    $status = false;
                } else {
                    $status = true;
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        try {
            $this->file_system->chmod($path, 0755);
        } catch (Exception $e) {
            $this->logger->error('CHMOD 755 ERROR ' . $path, [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $baseDir = $ENV['CAMPAIGN_DIR']
     * @return int
     * @author jsr
     */
    private function getBaseDirSegments(string $baseDir): int
    {
        return (count(explode(DIRECTORY_SEPARATOR, $baseDir)) - 1);
    }


    /**
     * @param string $path
     * @param string $dir
     * @return array
     * @author jsr
     */
    private function setPathPerms(string $path, string $dir): array
    {
        $err = [];
        $baseDirSegments = $this->getBaseDirSegments($this->dir);
        $pathSegments = explode(DIRECTORY_SEPARATOR, $path . DIRECTORY_SEPARATOR . $dir);
        unset($pathSegments[0]); // remove empty segment
        $basePath = '';

        foreach ($pathSegments as $key => $segment) {
            echo "\n$key) $segment";
            if ($key <= $baseDirSegments) {
                $basePath .= DIRECTORY_SEPARATOR . $segment;
            } else {
                try {
                    $this->file_system->chmod($basePath . DIRECTORY_SEPARATOR . $segment, 0755);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                    $err[] = 'chmod 775 failed for ' . $basePath . DIRECTORY_SEPARATOR . $segment . ': ' . $e->getMessage();
                }
                $basePath .= DIRECTORY_SEPARATOR . $segment;
            }
        }
        return $err;
    }


    /**
     * @param string $src
     * @param string $destination
     * @return bool
     * @author pva
     */
    private function copyCampaignFolder(string $src, string $destination): bool
    {
        $status = false;
        if (empty($src) || empty($destination) || !$this->exists($src) || !$this->exists($destination)) {
            $this->logger->critical('Invalid Folder for copy', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $this->logger->info("MIRROR: $src to $destination", [__METHOD__, __LINE__]);
            $this->file_system->mirror($src, $destination);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return true;
    }


    /**
     * @param string $camp_name
     * @param string $company
     * @param string $version
     * @param string $campaignSettings
     * @return bool
     * @author pva | Aki
     */
    public function activateCampaign(string $camp_name, string $company, string $version, string $campaignSettings): bool
    {
        $this->logger->info('ACTIVATECAMPAIGN: ', ['$camp_name' => $camp_name, '$company' => $company, '$version' => $version], [__METHOD__, __LINE__]);

        $status = false;
        if (empty($camp_name) || empty($company)) {
            $this->logger->critical('Invalid Params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $company_path = $this->dir . DIRECTORY_SEPARATOR . $company;
            $version_path = $company_path . DIRECTORY_SEPARATOR . $camp_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR;
            //adding campaign settings file while activation
            if ($this->addSettingsFileAndFormFieldsToCampaign($version_path, $campaignSettings) === false) {
                $this->logger->critical('Settings file  not created', [__METHOD__, __LINE__]);
                return $status;
            }

            $active_path = $company_path . DIRECTORY_SEPARATOR . $camp_name . DIRECTORY_SEPARATOR;
            if ($this->exists($version_path) && $this->copyCampaignFolder($version_path, $active_path)) {
                $status = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $camp_name
     * @param string $company
     * @param string $old_version
     * @param string $new_version
     * @param string $token
     * @return bool
     */
    public function reviseCampaign(string $camp_name, string $company, string $old_version, string $new_version, string $token): bool
    {
        $status = false;
        if (empty($camp_name) || empty($company) || empty($old_version) || empty($new_version)) {
            $this->logger->critical('Invalid Params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $company_path = $this->dir . DIRECTORY_SEPARATOR . $company;
            $version_path = $company_path . DIRECTORY_SEPARATOR . $camp_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR;
            $old_version_path = $company_path . DIRECTORY_SEPARATOR . $camp_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $old_version . DIRECTORY_SEPARATOR;
            $new_version_path = $company_path . DIRECTORY_SEPARATOR . $camp_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $new_version . DIRECTORY_SEPARATOR;
            if ($this->createDir($version_path, $new_version) && $this->copyCampaignFolder($old_version_path, $new_version_path)) {
                $status = true;
            }
            if ($status == true) {
                //Append Token to File.
                $this->file_system->dumpFile($new_version_path . '/token.txt', $token);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $company
     * @param string $campaign_name
     * @param string $version
     * @param string $splittest_name
     * @return bool
     * @author Pradeep
     * @author aki
     */
    public function createSplittest(
        string $company,
        string $campaign_name,
        string $version,
        string $splittest_name
    ): bool
    {
        $status = false;
        if (empty($campaign_name) || empty($company) || empty($version) || empty($splittest_name)) {
            $this->logger->critical('Invalid Params passed', [__METHOD__, __LINE__]);
            return $status;
        }

        $company_path = $this->dir . DIRECTORY_SEPARATOR . $company;
        $version_path = $company_path . DIRECTORY_SEPARATOR . $campaign_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR;
        if (!$this->createDir($version_path, $splittest_name)) {
            $this->logger->critical('Unable to create splittest folder at' . $version_path, [__METHOD__, __LINE__]);
            return $status;
        }

        $i = 1;
        $splittest_path = $version_path . DIRECTORY_SEPARATOR . $splittest_name . DIRECTORY_SEPARATOR;
        while ($i <= 10) {
            // Create variant folder.
            // By default create max 10 folders.
            if (!$this->createDir($splittest_path, $i)) {
                $this->logger->critical('Unable to create variant folder ' . $splittest_path . DIRECTORY_SEPARATOR . $i, [__METHOD__, __LINE__]);
                $i++;
                continue;
            }
            $campaign_path = $company_path . DIRECTORY_SEPARATOR . $campaign_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR;
            $template_path = $campaign_path . 'templates/splittest.html';
            $variant_path = $splittest_path . DIRECTORY_SEPARATOR . $i;
            $variant_file = 'index.html';
            $this->file_system->dumpFile($variant_path . DIRECTORY_SEPARATOR . $variant_file, '');
            //copy contents in the template
            try {
                $this->file_system->copy($template_path, $variant_path . DIRECTORY_SEPARATOR . $variant_file, true);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            }

            $i++;
        }

        return $status;
    }


    /**
     * @param string $company
     * @param string $campaign_old
     * @param string $version
     * @param string $campaign_new
     * @param string $token
     * @return bool
     * @author Pradeep
     */
    public function copyCampaign(
        string $company,
        string $campaign_old,
        string $version,
        string $campaign_new,
        string $token
    ): bool
    {
        $status = false;
        if (empty($company) || empty($campaign_old) || empty($campaign_new) || empty($version)) {
            return $status;
        }
        $company_path = $this->dir . DIRECTORY_SEPARATOR . $company . DIRECTORY_SEPARATOR;
        $old_campaign_path = $company_path . $campaign_old . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR;
        $rel_campaign_path = $campaign_new . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . '1' . DIRECTORY_SEPARATOR;
        $new_campaign_path = $company_path . $rel_campaign_path;
        if ($this->createDir($company_path, $rel_campaign_path) && $this->copyCampaignFolder($old_campaign_path, $new_campaign_path)) {
            //Append Token to File.
            $this->file_system->dumpFile($new_campaign_path . DIRECTORY_SEPARATOR . 'token.txt', $token);
            $status = true;
        }
        return $status;
    }


    /**
     * @param string $campaign_name
     * @param string $company
     * @param string $version
     * @param array $additional_pages
     * @return bool
     * @author Pradeep
     * @author aki
     */
    public function createAdditionalPages(
        string $campaign_name,
        string $company,
        string $version,
        array  $additional_pages
    ): bool
    {
        if (empty($campaign_name) || empty($company) || empty($additional_pages) || empty($version)) {
            return false;
        }
        try {
            $company_path = $this->dir . DIRECTORY_SEPARATOR . $company . DIRECTORY_SEPARATOR;
            $campaign_path = $company_path . $campaign_name . DIRECTORY_SEPARATOR . 'versions' . DIRECTORY_SEPARATOR . $version . DIRECTORY_SEPARATOR;
            $template_path = $campaign_path . 'templates/template.html';
            $this->logger->info(json_encode($additional_pages));
            try {
                $this->file_system->chmod($template_path, 0777);
            } catch (Exception $e) {
                $this->logger->error('CHMOD 777 ERROR ' . $template_path, [__METHOD__, __LINE__]);
            }
            foreach ($additional_pages as $page) {
                if (empty($page) || empty($page['name']) || ($page['name'] === 'index.html')) {
                    $this->logger->info('empty page or index.html ' . $page['name']);
                    continue;
                }
                $additional_page_path = $campaign_path . DIRECTORY_SEPARATOR . $page['name'];
                if ($this->exists($additional_page_path)) {
                    $this->logger->info('page already exits ' . $page['name']);
                    continue;
                }
                $this->logger->info('page does not exits ' . $page['name']);
                $this->file_system->dumpFile($campaign_path . 'template.html', '');
                //copy file
                try {
                    //copy template
                    $this->file_system->copy($template_path, $campaign_path . DIRECTORY_SEPARATOR . 'template.html', true);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                }
                //change Name of the template
                $template_name = $campaign_path . DIRECTORY_SEPARATOR . 'template.html';
                $required_name = $campaign_path . $page['name'];
                try {
                    //change name
                    $this->file_system->rename($template_name, $required_name);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
                }
                $this->logger->info('page created ' . $page['name']);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return true;
    }


    /**
     * @param array $pageDetails
     * @author Aki
     */
    public function getZipFile(array $pageDetails)
    {
        $host_dir = $_ENV['CAMPAIGN_DIR'];
        if (!$this->exists($host_dir)) {
            return null;
        }
        $zipName = $pageDetails['folder_name'] . '.zip';
        $zip = new ZipArchive;
        if ($zip->open($zipName, ZipArchive::CREATE) === true) {
            var_dump('comes hear2212');
            // Add a file new.txt file to zip using the text specified
            $zip->addFromString('new.txt', 'text to be added to the new.txt file');

            // All files are added, so close the zip file.
            $zip->close();
        }
        return $zip;
    }


    /**
     * @param array $pageDetails
     * @return string
     * @author Aki
     */
    public function generatePathForFolder(array $pageDetails): string
    {
        $dir = $this->getDirPath();
        $account = $pageDetails['company'];
        $folder = $dir . "/" . $account . DIRECTORY_SEPARATOR . $pageDetails['folder_name'];
        if ($pageDetails['page_type'] === 'draft') {
            $path = $folder . DIRECTORY_SEPARATOR . "versions" . DIRECTORY_SEPARATOR . $pageDetails['version'];
        } else {
            $path = $folder;
        }
        return $path;
    }


    /**
     * @param string $relativePath
     * @return bool
     * @author Aki
     */
    public function deleteFiles(string $relativePath): bool
    {
        $dir = $this->getDirPath();
        $folder = $dir . DIRECTORY_SEPARATOR . $relativePath; // /var/www/.../versions/<n>/

        $this->logger->info('PARAMS', ['$relativePath' => $relativePath, '$dir' => $dir, '$folder' => $folder, __METHOD__, __LINE__]);

        $this->logger->info('VARS-' . __FUNCTION__, [$relativePath, $dir, $folder]);
        // VARS-deleteFiles ["1002/eefw/versions/2","/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net","/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/1002/eefw/versions/2"]
        //use iterators
        $files = new DirectoryIterator($folder);
        $javascriptFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder . DIRECTORY_SEPARATOR . "script", FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST);
        $styleFiles = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder . DIRECTORY_SEPARATOR . "css", FilesystemIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST);
        /*
         request.CRITICAL: Uncaught PHP Exception UnexpectedValueException: "RecursiveDirectoryIterator::__construct(/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/1002/TEST_3/versions/1/css): failed to open dir:
        No such file or directory" at /var/www/vhosts/dev-campaign-in-one.net/mscamp.dev-campaign-in-one.net/src/Servies/FileSystemServices.php line 658
        {"exception":"[object] (UnexpectedValueException(code: 0): RecursiveDirectoryIterator::__construct(/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/1002/TEST_3/versions/1/css):
        failed to open dir: No such file or directory at /var/www/vhosts/dev-campaign-in-one.net/mscamp.dev-campaign-in-one.net/src/Servies/FileSystemServices.php:658)"}
         */

        //delete all html files
        try {
            foreach ($files as $file) {
                $this->logger->info('file', [$file, __METHOD__, __LINE__]);
                if (!$file->isDot()) {
                    $fileExtension = $file->getExtension();
                    if ($fileExtension === 'html') {
                        unlink($file->getPathname());
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error('HTML files not deleted for upload pages', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }

        //delete script files along with directory
        try {
            foreach ($javascriptFiles as $file) {
                $file->isDir() ? rmdir($file) : unlink($file);
            }
            rmdir($folder . DIRECTORY_SEPARATOR . "script");
        } catch (Exception $e) {
            $this->logger->error('script files not deleted for upload pages', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }

        //delete css files along with directory
        try {
            foreach ($styleFiles as $file) {
                $file->isDir() ? rmdir($file) : unlink($file);
            }
            rmdir($folder . "/css");
        } catch (Exception $e) {
            $this->logger->error(' CSS files not deleted for upload pages', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }

        return true;
    }


    /**
     * @param string $sourceFolder
     * @param string $targetRelativePath
     * @return bool
     * @author jsr
     */
    public function moveUnzipedFiles(string $sourceAbsolutePath, string $targetRelativePath): bool
    {
        $this->logger->info('PARAMS', ['$sourceAbsolutePath' => $sourceAbsolutePath, '$targetRelativePath' => $targetRelativePath, __METHOD__, __LINE__]);
        $dir = $this->getDirPath();
        $targetPath = $dir . DIRECTORY_SEPARATOR . $targetRelativePath;

        // $this->logger->info('CANNOT OPEN', [$sourceAbsolutePath . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'Classes', __METHOD__, __LINE__]);

        $copy = [
            [
                'directoryIterator' => new DirectoryIterator($sourceAbsolutePath),
                'fileExtension' => 'html',
                'relativeTargetPath' => '',
            ],
            [
                'directoryIterator' => new DirectoryIterator($sourceAbsolutePath . DIRECTORY_SEPARATOR . 'scripts'),
                'fileExtension' => 'js',
                'relativeTargetPath' => DIRECTORY_SEPARATOR . 'scripts',
            ],
// Classes directory was removed below directory files/upload_pages/uploadtest. WEB-5662
//            [
//                //'directoryIterator' => new DirectoryIterator($sourceAbsolutePath . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'Classes'),
//                'directoryIterator' => new DirectoryIterator($sourceAbsolutePath . DIRECTORY_SEPARATOR . 'script' ),
//                'fileExtension' => 'js',
//                //'relativeTargetPath' => DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'Classes',
//                'relativeTargetPath' => DIRECTORY_SEPARATOR . 'script',
//            ],
            [
                'directoryIterator' => new DirectoryIterator($sourceAbsolutePath . DIRECTORY_SEPARATOR . 'styles'),
                'fileExtension' => 'css',
                'relativeTargetPath' => DIRECTORY_SEPARATOR . 'styles',
            ],
        ];

        $status = [];
        try {
            foreach ($copy as $key => $cp) {
                $status[$key] = $this->simpleCopy($cp['directoryIterator'], $cp['fileExtension'], $targetPath, $cp['relativeTargetPath']);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            throw new Exception($e->getMessage());
        }

        return (in_array(false, $status)) ? false : true;
    }


    /**
     * @param DirectoryIterator $directoryIterator
     * @param string $fileExtension
     * @param string $targetPath
     * @param string $relativeTargetPath
     * @return bool
     * @author jsr
     * @internal Loops through the directory and search the file extension and copy to target path + relative target path
     */
    private function simpleCopy(DirectoryIterator $directoryIterator,
                                string $fileExtension,
                                string $targetPath,
                                string $relativeTargetPath
    ): bool
    {
        $status = [];
        try {
            foreach ($directoryIterator as $file) {

                if (!$file->isDot()) {
                    if ($fileExtension === $file->getExtension()) {
                        // echo "\n" .  "copy({$file->getPathname()}, {$targetPath}{$relativeTargetPath}".DIRECTORY_SEPARATOR."{$file->getFilename()} )";
                        // copy(/var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/files/upload_pages/TEST_3/script/route.js, /var/www/vhosts/dev-campaign-in-one.net/campaigns.dev-campaign-in-one.net/1002/TEST_3/versions/1/script/route.js ) ["simpleCopy",768] []
                        $this->logger->info("copy({$file->getPathname()}, {$targetPath}{$relativeTargetPath}" . DIRECTORY_SEPARATOR . "{$file->getFilename()} )", [__FUNCTION__, __LINE__]);
                        $status[$file->getPathname()] = copy($file->getPathname(), $targetPath . $relativeTargetPath . DIRECTORY_SEPARATOR . $file->getFilename());
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error("$fileExtension files could not be copied", [$e->getMessage(), __METHOD__, __LINE__]);
            throw new Exception("$fileExtension files could not be copied. Check " . json_encode($status));
        }

        return (in_array(false, $status)) ? false : true;
    }


    /**
     * @param string $sourceFolder
     * @param string $targetRelativePath
     * @return string|null
     * @author Aki, jsr
     */
    public function unZip(string $sourceFolder, string $zipFileName, string $uploadDir): ?string
    {
        $this->logger->info('PARAMS', ['$sourceFolder' => $sourceFolder, '$zipFileName' => $zipFileName, '$uploadDir' => $uploadDir, __METHOD__, __LINE__]);

        $unzipStatus = false;
        $targetAbsolutePath = $this->getDirPath() . preg_replace('/\.\./', '', $uploadDir);

        $zipFile = $sourceFolder . DIRECTORY_SEPARATOR . $zipFileName;
        $this->logger->info('PARAMS', ['$targetAbsolutePath' => $targetAbsolutePath, '$zipFile' => $zipFile, __METHOD__, __LINE__]);
        //extract the files
        try {
            $zip = new ZipArchive;
            if ($zip->open($zipFile, ZipArchive::CREATE) === true) {
                $unzipStatus = $zip->extractTo($targetAbsolutePath);
                $zip->close();
            }
        } catch (Exception $e) {
            $this->logger->error("Could not unzip $zipFile", [$e->getMessage(), __METHOD__, __LINE__]);
            return null;
        }
        return ($unzipStatus) ? $targetAbsolutePath : null;
    }


    /**
     * @param string $uploadDir
     * @return bool
     * @author Aki
     */
    public function deleteTempFile(string $uploadDir): bool
    {
        try {
            $directory = new RecursiveDirectoryIterator($uploadDir, FilesystemIterator::SKIP_DOTS);
            $files = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ($files as $file) {
                $file->isDir() ? rmdir($file) : unlink($file);
            }
        } catch (Exception $e) {
            $this->logger->error('temp files not deleted', [$e->getMessage(), __METHOD__, __LINE__]);
            return false;
        }

        return true;
    }


    /**
     * @return string
     * @author pva
     */
    private function getJsClientPath(): string
    {
        $js_client_path = '';
        $dir = $this->getDirPath();
        if ($this->exists($dir)) {
            $js_client_path = $dir . 'js_client';
        }
        $this->logger->info('campaign path ' . $js_client_path);
        return $js_client_path;
    }


    /**
     * @return string
     * @author pva
     */
    public function getDirPath(): string
    {
        $host_dir = $_ENV['CAMPAIGN_DIR'];
        $this->logger->info('dir path ' . $host_dir);
        if (!$this->exists($host_dir)) {
            return '';
        }
        return $host_dir;
    }


    /**
     * @param string $dir_path
     * @param string $folder_name
     * @return bool
     */
    private function createFolder(string $dir_path, string $folder_name): bool
    {
        $status = false;
        if (empty($dir_path) || empty($folder_name)) {
            $this->logger->critical('Invalid Params passed', [__METHOD__, __LINE__]);
            return $status;
        }
        try {
            $new_path = $dir_path . DIRECTORY_SEPARATOR . $folder_name;
            $this->file_system->mkdir($new_path, 0755);
            $status = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $status;
    }


    /**
     * @param string $path
     * @param string $camp_name
     * @return bool
     * @author pva
     */
    private function isCampaignCopied(string $path, string $camp_name): bool
    {
        $status = false;
        if (empty($path) || !$this->exists($path) || empty($camp_name)) {
            return $status;
        }
        if ($this->exists($path . DIRECTORY_SEPARATOR . 'index.php')) {
            $status = true;
        }

        return $status;
    }


    /**
     *
     * @param string $versionPath
     * @param string $camp_settings
     * @return bool
     * @author Aki
     */
    public function addSettingsToCampaign(string $versionPath, string $camp_settings): bool
    {
        $this->logger->info('Add settings: ', ['$camp_name' => $camp_settings], [__METHOD__, __LINE__]);

        $status = false;
        $addingDivStatus = false;
        $fileDeleteStatus = false;
        $copyHtmlFileStatus = false;
        $htmlPath = $versionPath . '/index.html';
        $surveyPath = $versionPath . '/survey.html';
        $formPath = $versionPath . '/form.html';
        //delete the existing html file
        try {
            $this->file_system->remove($htmlPath);
            $fileDeleteStatus = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        //copy template of html file
        $template_path = $versionPath . 'templates/indexTemplate.html';
        $this->file_system->dumpFile($versionPath . 'index.html', '');
        try {
            //copy template
            $this->file_system->copy($template_path, $versionPath . '/index.html', true);
            $copyHtmlFileStatus = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        //add new div to html file
        $div = '<div id="campaignSettings" data-campaignSettings="' . base64_encode($camp_settings) . '"></div>';
        try {
            $this->file_system->appendToFile($htmlPath, $div);
            $addingDivStatus = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        if ($addingDivStatus && $fileDeleteStatus && $copyHtmlFileStatus) {
            $status = true;
        }
        return $status;
    }


    /**
     *
     * @param string $versionPath
     * @param string $camp_settings
     * @return bool
     * @author Aki
     */
    public function addSettingsFileAndFormFieldsToCampaign(string $versionPath, string $camp_settings): bool
    {
        $this->logger->info('Add settings: ', ['$camp_name' => $camp_settings], [__METHOD__, __LINE__]);
        $status = false;

        $encoded_campaign_settings = base64_encode($camp_settings);
        try {
            $this->file_system->dumpFile($versionPath . '/settings.txt', $encoded_campaign_settings);
            $status = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        return $status;
    }


    /**
     * @param string $fileName e.g. index.html
     * @param string $startSearchFromDirectory e.g. /upload/temp
     * @return string absolute path where file was found or null
     * @author jsr
     */
    public function getAbsolutePath(string $fileName, string $startSearchFromDirectory): ?string
    {
        try {
            $directoryIterator = new RecursiveDirectoryIterator($startSearchFromDirectory);
        } catch (Exception $e) {
            $expectedFileNameArr = explode(DIRECTORY_SEPARATOR, $startSearchFromDirectory);
            $expectedFileName = $expectedFileNameArr[count($expectedFileNameArr) - 1];
            throw new Exception("The uploaded zip file does not contain a root folder '{$expectedFileName}'.  Please check your zip acrhive.");
        }

        foreach (new RecursiveIteratorIterator($directoryIterator) as $pathAndFile) {
            if (stristr($pathAndFile, $fileName) !== false) {
                return rtrim(preg_replace("/{$fileName}/", '', $pathAndFile), DIRECTORY_SEPARATOR);
            }
        }
        return null;
    }

    /**
     * @param string $versionPath
     * @param string $renderedHtmlForm
     * @return void
     * @author Aki
     */
    public function addMappingFieldsToFormHtml(string $versionPath, string $renderedHtmlForm): void
    {

        try {
            $this->file_system->dumpFile($versionPath . '/form.html', $renderedHtmlForm);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

    }
}
