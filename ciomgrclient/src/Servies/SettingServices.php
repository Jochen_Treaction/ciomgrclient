<?php


namespace App\Servies;


use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @author aki
 * Class CampaignSettingServices
 * @package App\Servies
 */
class SettingServices
{


    /**
     * @var LoggerInterface
     */
    private $logger;

    //public variables for file names
    public const INDEX_HTML = "index.html";
    public const SUCCESS_PAGE = "success.html";
    public const ERROR_PAGE = "error.html";
    public const GEOBLOCKING_PAGE = "geoblocking.html";




    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $mapping
     * @return array
     * @author Aki
     */
    public function formatMappingForTwig(array $mapping):array
    {
        //combine custom fields and standard fields
        $standardFields = $mapping['contact']['standard'];
        $customFields = $mapping['contact']['custom'];
        // $this->logger->info('fieldsarray', [$standardFields, $customFields, __METHOD__, __LINE__]);
        return array_merge($standardFields,$customFields);
    }

    /**
     * @param string $pagSettings
     * @return array
     * @throws \JsonException
     * @author Aki
     */
    public function formatDataForTwig(string $pagSettings):array
    {
        //getting mapping table from settings
        $mappingTable = json_decode($pagSettings, true, 512,JSON_THROW_ON_ERROR)['mapping']['mappingTable'];
        $this->logger->info('mapping table', [$mappingTable, __METHOD__, __LINE__]);
        //Decoding base64 encoded mapping
        $mapping = json_decode(base64_decode($mappingTable), true, 512, JSON_THROW_ON_ERROR);
        $this->logger->info('mapping', [$mapping, __METHOD__, __LINE__]);
        //Format the mapping to twig friendly structure
        $formatMappingForTwig = $this->formatMappingForTwig($mapping);
        $this->logger->info('formatted Mapping for twig', [$formatMappingForTwig, __METHOD__, __LINE__]);
        //return
        return $formatMappingForTwig;
    }

    /**
     * @return array
     * @author aki
     * @internal creates array of required files for download
     */
    public function filesAllowedForDownload():array
    {
        return array(
            self::INDEX_HTML,
            self::ERROR_PAGE,
            self::SUCCESS_PAGE,
            self::GEOBLOCKING_PAGE,
        );
    }

}
